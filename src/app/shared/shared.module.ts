import { ItemListComponent } from './../item-list/item-list.component';
import { ItemComponent } from './../item/item.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatTableModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatSortModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatInputModule,
  MatFormFieldModule,
  MatCardModule,
  MatSlideToggleModule,
  MatDividerModule
 } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MyTableComponent } from '../my-table/my-table.component';


@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatSlideToggleModule,
    MatDividerModule
  ],
  declarations: [
    ItemComponent,
    ItemListComponent,
    MyTableComponent
   ],
  exports: [
    ItemComponent,
    MyTableComponent,
    LayoutModule,
    ItemListComponent,
    CommonModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatSlideToggleModule,
    MatDividerModule
  ]
 })
 export class SharedModule { }
