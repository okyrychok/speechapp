import { ToBuyItemModel } from '../models/to-buy-item-model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {

  constructor() { }

  @Input() item: ToBuyItemModel;
  @Output() updateItem: EventEmitter<ToBuyItemModel> = new EventEmitter();
  @Output() deleteItem: EventEmitter<ToBuyItemModel> = new EventEmitter();

  action: string = this.item  ? 'Buy' : 'Sell';

  update(item: ToBuyItemModel) {
    this.updateItem.emit(item);
  }

  delete(item: ToBuyItemModel) {
    this.deleteItem.emit(item);
  }

}
