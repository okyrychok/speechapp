import { Component, OnInit } from '@angular/core';
import { FireBaseApiService } from '../fireBaseApi.service';
import { ToBuyItemModel } from '../models/to-buy-item-model';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-dollars',
  templateUrl: './dollars.component.html',
  styleUrls: ['./dollars.component.css']
})
export class DollarsComponent implements OnInit {
  showToBuy = true;
  sliderTitle = (this.showToBuy === true) ? 'Купівля' : 'Продаж';
  constructor(private fireBaseApiService: FireBaseApiService) {
    this.fireBaseApiService.initDB();
  }

  get ToBuyList() {
    const tobuyListItems: ToBuyItemModel[] = [];

    this.fireBaseApiService.getDBdataObservable()
    .pipe(filter(item =>
         (item.action.indexOf('куплю') >= 0)
      && (item.currency.indexOf('долла') >= 0)))
    .subscribe(item => tobuyListItems.push(item));
    return tobuyListItems;
  }

  get ToSellList() {
    const tosellListItems: ToBuyItemModel[] = [];

    this.fireBaseApiService.getDBdataObservable()
    .pipe(filter(item =>
         (item.action.indexOf('продам') >= 0)
      && (item.currency.indexOf('долла') >= 0)))
    .subscribe(item => tosellListItems.push(item));
    return tosellListItems;
  }

  remove(item: ToBuyItemModel) {
    this.fireBaseApiService.remove(item.id);
  }

  onChange(value) {
    if (value.checked === true) {
      this.sliderTitle = 'Купівля';
      this.showToBuy = value.checked;
    } else {
      this.showToBuy = value.checked;
      this.sliderTitle = 'Продаж';
    }
  }

  ngOnInit() {
  }

}
