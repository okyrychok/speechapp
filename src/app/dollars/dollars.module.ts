import { SharedModule } from './../shared/shared.module';
import { ItemListComponent } from './../item-list/item-list.component';
import { ItemComponent } from './../item/item.component';
import { FireBaseApiService } from './../fireBaseApi.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DollarsRoutingModule } from './dollars-routing.module';
import { DollarsComponent } from './dollars.component';

@NgModule({
  imports: [
    CommonModule,
    DollarsRoutingModule,
    SharedModule
  ],
  declarations: [
    DollarsComponent // ,
    // ItemComponent,
    // ItemListComponent
  ],
  providers: [FireBaseApiService]
})
export class DollarsModule { }
