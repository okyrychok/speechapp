import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Observer } from 'rxjs';

@Injectable()
export class AuthentificationService {
  loginUser (username: string, password: string) {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      if (username === 'test' && password === 'test') {
        localStorage.setItem('currentUser', username + password );
      }
      return observer.next(true);
    });
  }

  logout() {
      localStorage.removeItem('currentUser');
  }

}
