import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './authguard/auth.guard';
import { MyTableComponent } from './my-table/my-table.component';

const routes: Routes = [
  {
    path: 'dollars',
    loadChildren: 'app/dollars/dollars.module#DollarsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'euros',
    loadChildren: 'app/euros/euros.module#EurosModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'homepage',
    loadChildren: 'app/homepage/homepage.module#HomepageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: 'app/login/login.module#LoginModule'
  },
  {
    path: 'table',
    component: MyTableComponent
  },
  {
    path: '',
    redirectTo: 'homepage',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
