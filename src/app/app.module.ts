import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

 import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { SpeechRecognitionService } from './speech-recognition.service';
// import { ItemListComponent } from './item-list/item-list.component';
// import { ItemComponent } from './item/item.component';
import { FireBaseApiService } from './fireBaseApi.service';
import { AuthGuard } from './authguard/auth.guard';
import { AuthentificationService } from './authentification.service';
import { HttpClientModule, HttpHandler } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { NavigationComponent } from './navigation/navigation.component';



@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
    // ItemListComponent,
    // ItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [SpeechRecognitionService, FireBaseApiService, AuthGuard, AuthentificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
