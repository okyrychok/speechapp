import { Injectable } from '@angular/core';

declare const webkitSpeechRecognition: any;
declare const setTimeout: any;

@Injectable()
export class SpeechRecognitionService {
  recognition: any;
  recognizing = false;
  start_timestamp: any;
  ignore_onend: boolean;

  final_transcript = '';

  speechResultListener: Function;

  clearTranslationTimeout: number;

  constructor() {}

  init() {
    const self = this;

    this.recognition = new webkitSpeechRecognition();
    this.recognition.continuous = true;
    this.recognition.interimResults = true;

    this.recognition.onstart = function() {
      self.recognizing = true;
    };

    this.recognition.onerror = function(event) {
      if (event.error === 'no-speech') {
        this.error = 'info_no_speech';
        self.ignore_onend = true;
      }
      if (event.error === 'audio-capture') {
        this.error = 'info_no_microphone';
        self.ignore_onend = true;
      }
      if (event.error === 'not-allowed') {
        if ((event.timeStamp - this.start_timestamp) < 100) {
          this.error = 'info_blocked';
        } else {
          this.error = 'info_denied';
        }
        self.ignore_onend = true;
      }
      alert(this.error);
    };

    this.recognition.onend = () => {
      console.log('onend');
      this.recognizing = false;
      if (self.ignore_onend) {
        return;
      }
      if (!self.final_transcript) {
        return;
      }
    };

    this.recognition.onresult = function(event) {

      let interim_transcript = '';

      for (let i = event.resultIndex; i < event.results.length; ++i) {
        if (event.results[i].isFinal) {
          self.final_transcript += event.results[i][0].transcript;
        } else {
          interim_transcript += event.results[i][0].transcript;
        }
      }

      if (self.final_transcript) {
         console.log('final_transcript = ' + self.linebreak(self.final_transcript));

        if (self.clearTranslationTimeout) {
          clearTimeout(self.clearTranslationTimeout);
        }

        self.clearTranslationTimeout = setTimeout(() => {
          self.final_transcript = '';
        }, 1000);


        if (self.speechResultListener) {
          self.speechResultListener(self.linebreak(self.final_transcript));
          self.final_transcript = null;
        }
      }
    };
  }

  /**
   * Start speech recognition
   */
  start() {
    if (this.recognizing) {
      this.recognition.stop();
      return;
    }

    this.final_transcript = '';
    this.recognition.lang = 'uk-UA';
    this.recognition.start();

    this.ignore_onend = false;

    this.start_timestamp = new Date();
  }

  stop() {
    this.recognition.stop();
    this.recognizing = false;
  }

  setSpeechParseResultListener(callback: Function) {
    this.speechResultListener = callback;
  }


  private linebreak(s) {
    const two_line = /\n\n/g;
    const one_line = /\n/g;

    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
  }
}
