import { ToBuyItemModel } from '../models/to-buy-item-model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent {
  @Input() itemList: Array<ToBuyItemModel>;
  @Output() updateItemList: EventEmitter<ToBuyItemModel> = new EventEmitter();
  @Output() deleteItemList: EventEmitter<ToBuyItemModel> = new EventEmitter();

  update(item: ToBuyItemModel) {
    this.updateItemList.emit(item);
  }

  delete(item: ToBuyItemModel) {
    this.deleteItemList.emit(item);
  }

}
