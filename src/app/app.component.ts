import { Component } from '@angular/core';
import { SpeechRecognitionService } from './speech-recognition.service';
import { FireBaseApiService } from './fireBaseApi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';

  constructor(private speechRecognitionService: SpeechRecognitionService
    // , private fireBaseApiService: FireBaseApiService
  ) {
    this.speechRecognitionService.init();
    // this.fireBaseApiService.initDB();
  }

  startlistening() {
    this.speechRecognitionService.start();
  }
}
