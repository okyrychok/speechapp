import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EurosRoutingModule } from './euros-routing.module';
import { EurosComponent } from './euros.component';
import { FireBaseApiService } from '../fireBaseApi.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    EurosRoutingModule,
    SharedModule
  ],
  declarations: [EurosComponent],
  providers: [FireBaseApiService]
})
export class EurosModule { }
