import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EurosComponent } from './euros.component';

const routes: Routes = [
  {
    path: '',
    component: EurosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EurosRoutingModule { }
