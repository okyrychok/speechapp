export interface ToBuyItemModel {

      id: number;
      action: string;
      amount: number;
      currency: string;
      checked: boolean;

}
