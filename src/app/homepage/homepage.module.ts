import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomepageRoutingModule } from './homepage-routing.module';
import { HomepageComponent } from './homepage.component';
import { FireBaseApiService } from '../fireBaseApi.service';
// import { ItemListComponent } from '../item-list/item-list.component';
// import { ItemComponent } from '../item/item.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HomepageRoutingModule,
    SharedModule
  ],
  declarations: [
    HomepageComponent // ,
    // ItemListComponent,
    // ItemComponent
  ],
  providers: [FireBaseApiService]
})
export class HomepageModule { }
