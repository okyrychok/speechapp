import { FireBaseApiService } from '../fireBaseApi.service';
import { ToBuyItemModel } from '../models/to-buy-item-model';
import { filter } from 'rxjs/operators';

import { Component, OnInit, ViewChild } from '@angular/core';
import { SpeechRecognitionService } from '../speech-recognition.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  Title = 'Main page module';
  confirmTitle: string;
  confirmText: string;

  constructor(private fireBaseApiService: FireBaseApiService, private speechRecognition: SpeechRecognitionService) {
    this.fireBaseApiService.initDB();
    this.speechParseResultListener = this.speechParseResultListener.bind(this);
  }

  get tobuyListObservableChecked() {
    const tobuyListItems: ToBuyItemModel[] = [];

    this.fireBaseApiService.getDBdataObservable()
    .subscribe(item => tobuyListItems.push(item));
    return tobuyListItems;
  }

  add(action?: string, amount?: number, currency?: string) {
    const toBuyItem: ToBuyItemModel = {
      id: Math.random(),
      currency: currency || 'newItem -' + (+(new Date())),
      amount: amount || Math.round(Math.random() * 100),
      checked: action === 'Куплю' ? true : false,
      action: action || 'Куплю'
    };

    this.fireBaseApiService.add(toBuyItem);
  }

  remove(item: ToBuyItemModel) {
    this.fireBaseApiService.remove(item.id);
  }

  startSpeech() {
    this.confirmTitle = 'Прийом заявки.';
    this.confirmText = 'Виберіть дію, кількість і валюту.';
    alert(this.confirmTitle + ' ' + this.confirmText);
    this.speechRecognition.start();
  }

  speechParseResultListener(result) {
    this.confirmText = '';

    const parsedValues = result.trim().match(/(.*)\s(\d*)\s(.*)/);

    if (parsedValues
        &&
        ((parsedValues[1].indexOf('куплю' || 'продам') >= 0)
        ||
        (parsedValues[1].indexOf('продам') >= 0))) {
      if (parsedValues[3].indexOf('дол') >= 0) {
        parsedValues[3] = 'доллари';
      } else if (parsedValues[3].indexOf('євр') >= 0) {
        parsedValues[3] = 'євро';
      } else {
        alert('Не можу розпізнати заявку.');
        this.speechRecognition.stop();
      }

      this.add(parsedValues[1], parseInt(parsedValues[2], 10), parsedValues[3]);
      this.confirmText = 'Заявку успішно додано.';
      alert(this.confirmText);
      this.speechRecognition.stop();
    } else {
      this.confirmText = 'Не можу розпізнати заявку.';
      alert(this.confirmText);
      this.speechRecognition.stop();
    }
  }

  ngOnInit() {
    this.speechRecognition.init();
    this.speechRecognition.setSpeechParseResultListener(this.speechParseResultListener);
  }

}
